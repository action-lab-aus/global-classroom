# 1 - Setting up Slack

The following information is useful if you are setting up Global Classroom for the first time. We'll run through everything from setting up a Slack workspace and adding the 'Global Classroom App' to inviting people and setting permissions.

### **1.1** - Create a Slack workspace

- Create a new Slack account if you do not already have one - [https://slack.com/get-started#/createnew](https://slack.com/get-started#/createnew)
- Create a new workspace specifically for Global Classroom - [https://slack.com/intl/en-gb/get-started](https://slack.com/intl/en-gb/get-started)
  - Don't invite anyone just yet
  - Create a 'support' channel

### **1.2** - Adding the Global Classroom app to Slack

- Visit [https://api.slack.com/](https://api.slack.com/)
- Click **Your Apps** in top right (https://api.slack.com/apps)[https://api.slack.com/apps]
- Click **Create an App**, don't worry the app is already built but we need to set it up and get some tokens etc.
  - Choose 'From an app manifest'
  - Select the workspace you just created
  - Enter app manifest
    - Select 'YAML' tab
    - Paste the contents of **/server/manifest.yaml** into the text area, this will set the required scopes and permissions for the app amongst other configuration
    - Click 'Create'
  - From 'Basic Information/Display Information'
    - Add app name, description, icon and colour
  - Click 'Install to Workspace' and confirm

### **1.3** - Update app tokens/credentials

- From 'Basic Information/App Credentials'
  - Update: SLACK_APP_ID, SLACK_CLIENT_ID, SLACK_CLIENT_SECRET, SLACK_SIGNING_SECRET (in **/server/.env**)
- From 'Basic Information/App-Level Tokens'
  - Click 'Generate Token and Scopes'
  - Set 'Token Name' to 'App'
  - Add 'connections:write' scope
  - Click 'Generate'
  - Update: SLACK_APP_TOKEN with generated token (in **/server/.env**)
- From 'OAuth Tokens for Your Workspace/OAuth & Permissions'
  - Update: SLACK_BOT_TOKEN (in **/server/.env**) with Bot User OAuth Token
- Update: SLACK_WORKSPACE_ID (in **/server/.env**)
- Update: VITE_SLACK_WORKSPACE_ID (in **/client/.env**)

### **1.4** - Enable app tabs

- From 'App Home/Show Tabs'
  - Enable 'Home Tab'
  - Enable 'Messages Tab'

### **1.5** - Setting up permissions

- Get your Slack userID (by clicking profile in your workspace - Copy Member ID) and add this to 'admins' section of **/server/config/app.yaml**
- Restart the Global Classroom client & server, visit dashboard and sign in with your Slack account
