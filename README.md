# README

## What is this?

This software integrates with Slack through the Slack API. It offers the ability to organise people (students, learners, co-workers etc.) into groups in a Slack workspace and schedule the delivery of content and activities to the groups over a period of time. This content can take many different forms such as text, links, images or documents. Activities such as ice-breakers, role assignments, team questions and polls can all be scheduled with ease.

---

## Project structure

### Client (/client)

The client app is built on Vue 3.X and TailwindCSS, the client app is solely for administration of the system/deployment, production of the content and scheduling. Students/learners/participants do not interact with the client app, all of their interaction is within Slack. Administrators can sign into the client app using their Slack account once their 'Slack ID' has been added to `/server/config/app.yaml` in the `admins` section.

### Server (/server)

- [Express.js](https://expressjs.com/) & [Bolt](https://api.slack.com/bolt) application
- Interfaces with database (MongoDB) to process due 'schedule items' periodically configured in `/server/cron.js`
- Listens for and responds to events from the Slack API (e.g. message, member_joined_channel, reaction added etc.) configured in `/server/listeners/*`

### Database

The project utilises a 'MongoDB' database which runs in 'Docker' (see /docker-compose.yml). This is where

If you're looking for a good local tool to observe the database state try MongoDB Compass (https://www.mongodb.com/try/download/compass).

---

## First time setup

- Install Docker

  - On macos - (https://docs.docker.com/desktop/mac/install/)

- Install ngrok for local dev

  - On macos - `brew install ngrok/ngrok/ngrok` (https://ngrok.com/docs/getting-started)
  - You will need an ngrok account, make sure the config is added to your local machine

- Install npm package dependencies
  - Move into server directory - `cd ./server`
  - From the server directory run - `npm install`
  - Move into client directory - `cd ./client`
  - From the client directory run - `npm install`

---

## How to run locally

- Ensure Docker running
- Call `docker-compose up -d` from project root

  - This starts the mongo instance

- Run `ngrok http 3000`
- Get latest manifest from https://app.slack.com/app-settings/WORKSPACE_ID/APP_ID/app-manifest
- Update the ngrok url (e.g. `XXXX-XX-X-XX-XX.eu.ngrok.io`) across the project
- Paste the updated manifest in https://app.slack.com/app-settings/WORKSPACE_ID/APP_ID/app-manifest
- Verify the URL (at top of page where manifest was pasted)
- Start the server by running `npm run dev` from `/server`
- Start the client by running `npm run dev` from `/client`
- Visit `https://localhost:3002` to view the client/frontend
- Launch Slack in browser - e.g. https://app.slack.com/client/WORKSPACE_ID

---

## Notes

- Tutorials
  - https://blog.logrocket.com/build-a-slackbot-in-node-js-with-slacks-bolt-api/
  -
- Runs on port 3000 locally
- Use Ngrok to forward connection (updated URLs in Slack App Manifest) - ngrok http 3000

---

## Functionality

- App home
  - Show schedule
- Onboard user (capture info through a modal)
- Slack OAuth for dashboard
- Create a team
- Invite users to a team
- Request to rename team
- Archive all team channels
- Setup workspace
  - Help channel
- Local config file
- Role assignment
- Role re-assignment
- Scheduler
  - Schedule content (text, markdown, images, documents doc, docx, pdf)
  - Schedule polls
  - Schedule team question activity
- Questions
  - Submit team questions
  - Moderate questions
  - Distribute team questions
  - Record reactions to team question posts to determine awards
  - Assign awards based on emoji votes
  - Present awards


Bug list and TODOs:

https://docs.google.com/document/d/1yTo4lXmk7aGXizloU26CRfEFEs4LsumW8r7pevPp4OQ/edit?usp=sharing