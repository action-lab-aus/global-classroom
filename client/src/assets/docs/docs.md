# Global Classroom

This software integrates with Slack through the Slack API. It offers the ability to organise people (students, learners, co-workers etc.) into groups in a Slack workspace and schedule the delivery of content and activities to the groups over a period of time. This content can take many different forms such as text, links, images or documents. Activities such as ice-breakers, role assignments, team questions and polls can all be scheduled with ease.

---

# 1 - Setup workspace

### **1.1** - Invite users

Invite students/learners/co-workers to your workspace via Slack. This can be done in the Slack workspace fron the left-hand panel. Make sure they don't have any elevated privileges at this stage.

### **1.2** - Add facilitators

Once the facilitators have joined the workspace you can retrieve their Slack User ID from the database or their Slack profile withing Slack (look for 'Copy Member ID').

In order to convert a user to a facilitator you will need to add their Slack Member ID to the Global Classroom config file (**/server/config/app.yaml**) in the facilitators section. (See 5.2)

### **1.3** - Configure team sizes

Set the ideal and minimum team sizes in the Global Classroom config file (**/server/config/app.yaml**).

- **/teams/minSize** - The minimum number of users in each team, provided there are enough users to meet this minimum
- **/teams/idealSize** - The target number of users in each team

---

# 2 - Running a course

### **2.1** - Team assignment

When all of the participants have been invited to Slack we can assign the teams. Ensure you have configured the ideal and minimum team sizes in the config (see 1.3).

To team assignment locate and click the **Assign teams** button at the top of the Overview tab of the dashboard.

This process will take a moment to;

- Select all the students
- Create a number of private Slack channels based on the ideal/min team sizes configured
- Each team channel will be given a unique name such as 'the-secret-eagles' which is generated from available options (/server/team-names.yaml)
- Students will be added to their assigned private team channel
- Facilitators and admins will also be added to team channels

Once complete the dashboard will refresh and the teams will be visible in the 'Overview' tab.
You are now ready to assign activities and distribute content to the teams.

### **2.2** - Ice-breakers

Ice-breakers are an **Activity** so they can be scheduled like any other from the _Schedule_ tab in the left-hand menu of the dashboard (See 3.X).

The content of the ice-breaker message can be set in **server/i18n.yaml** by updating **iceBreaker/message**. This is a good opportunity to set the context of the course, introduce the participants to their team, set a challenge and encourage team introductions.

### **2.3** - Role assignment

Role assignment is an **Activity** so it can be scheduled like any other from the _Schedule_ tab in the left-hand menu of the dashboard (See 3.X).

Role descriptions can be configured in the **roles** section of **server/i18n.yaml**.

Every team will have one _leader_, one _writer_ and one or more _researchers_ and _evaluators_.

When the role assignment activity runs it will;

- Randomly allocate the roles within each team
- Send an automated message describing the roles and indicating who has been assigned to which role

Roles for each user will appear alongside each team member when viewing a team in the 'Overview' tab of the dashboard.

### **2.4** - Polls

Polls are an **Activity** so they can be scheduled like any other from the _Schedule_ tab in the left-hand menu of the dashboard (See 3.X).

A poll can have any number of options, the default is two but more can be added by clicking the 'plus' button below the options list.

When a poll is posted, team members can vote on the options with Slack. Responses are visible to other members of the team but not visible to other teams.

A user can change their vote at any point.

### **2.5** - Team Questions

Team Questions are an **Activity** so they can be scheduled like any other from the _Schedule_ tab in the left-hand menu of the dashboard (See 3.X).

The _Team Questions_ activity consists of three parts;

- Submission - a request for new questions is sent to each team, anyone from a team can submit a question before the given deadline
- Distribution - the questions are distributed between the teams and participants should discuss the question. They can also optionally vote (by adding an emoji reaction to the post) on wether it should be nominated for a question award (defined in **_/server/content/question-awards.yaml_**)
- Awards - awards are generated and distributed to the teams who submitted a question that was nominated for an award

### **2.6** - Content delivery

**Content** can be scheduled from the _Schedule_ tab in the left-hand menu of the dashboard (See 3.X).

Content can consist of markdown text, images and files. There are pre-defined content types which can be selected in the editor, these are defined in **server/content/content-types.yaml** and are used to add an image banner/prompt to the distributed message.

---

# 3 - Schedule

### **3.1** - Scheduling content & activities

When content or activities are created they will appear in the 'Drafts' tab of the 'Schedule' view of the dashboard.

From here either;

- A 'Post at' date can be set - this will schedule the content/activity to be posted at a set time and date
- The content/activity can be scheduled to post instantly

### **3.2** - Adjusting schedule

Click the 'trash' button in the top-right corner of a scheduled item to remove it from the schedule. The item will appear in the 'Drafts' tab where the 'Post at' date can be changed and re-scheduled.

### **3.3** - Viewing history

Click the 'Past' tab on the right-hand side of the 'Schedule' view. This is where all of the posted content and activities will be listed along with the date and time they were sent.

---

# 4 - Permissions

### **4.1** - Adding & removing admins

Please note there must be at least one admin (probably you)

- Add/remove the Slack UserID of the admin to/from the 'admins' section of **/server/config/app.yaml**
- Restart Global Classroom server app

### **4.2** - Adding & removing facilitators

- Add/remove the Slack UserID of the facilitator to/from the 'facilitators' section of **/server/config/app.yaml**
- Restart Global Classroom server app
