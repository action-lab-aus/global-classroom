import * as config from "@/config.js";
import axios from "axios";

export default {
  getUsers(type, cb, errorCb) {
    axios
      .get(`${config.API}/api/users/${type}`, { withCredentials: true })
      .then(
        (response) => {
          cb(response.data.data.users);
        },
        (response) => {
          errorCb(response);
        }
      );
  },
  getUser(id, cb, errorCb) {
    axios.get(`${config.API}/api/user/${id}`, { withCredentials: true }).then(
      (response) => {
        cb(response.data.data.user);
      },
      (response) => {
        errorCb(response);
      }
    );
  },
  syncUsers(cb, errorCb) {
    axios.get(`${config.API}/api/users/sync`, { withCredentials: true }).then(
      (response) => {
        cb(response.data.data);
      },
      (response) => {
        errorCb(response);
      }
    );
  },
};
