import * as config from '@/config.js';
import axios from 'axios';

export default {
  logout(cb, errorCb) {
    axios
      .get(`${config.API}/auth/logout`, { withCredentials: true })
      .then((response) => {
        cb(response.data.data);
      })
      .catch((error) => {
        errorCb(error);
      });
  },
  getStatus(cb, errorCb) {
    axios
      .get(`${config.API}/api/status`, { withCredentials: true })
      .then((response) => {
        if (response.data.meta.status == '401') {
          // errorCb();
          document.location = '/login';
          return;
        }

        cb(response.data.data);
      })
      .catch((error) => {
        errorCb(error);
      });
  },
};
