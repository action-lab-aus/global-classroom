import * as config from '@/config.js';
import axios from 'axios';

export default {
  getDraftItems(cb, errorCb) {
    axios.get(`${config.API}/api/schedule/items/draft`, { withCredentials: true }).then(
      (response) => {
        cb(response.data.data.items);
      },
      (response) => {
        errorCb(response);
      },
    );
  },
  getScheduledScheduleItems(cb, errorCb) {
    axios.get(`${config.API}/api/schedule/items/scheduled`, { withCredentials: true }).then(
      (response) => {
        cb(response.data.data.items);
      },
      (response) => {
        errorCb(response);
      },
    );
  },
  scheduleItem(_id, postAt, cb, errorCb) {
    axios
      .post(
        `${config.API}/api/schedule/items/schedule`,
        { _id, postAt },
        {
          withCredentials: true,
        },
      )
      .then(
        (response) => {
          cb(response.data.meta.msg);
        },
        (response) => {
          errorCb(response);
        },
      );
  },
  removeScheduleItem(_id, cb, errorCb) {
    axios
      .post(
        `${config.API}/api/schedule/items/remove`,
        { _id },
        {
          withCredentials: true,
        },
      )
      .then(
        (response) => {
          cb(response.data.meta.msg);
        },
        (response) => {
          errorCb(response);
        },
      );
  },
  createContent(body, cb, errorCb) {
    axios
      .post(`${config.API}/api/schedule/items/create`, body, {
        withCredentials: true,
      })
      .then(
        (response) => {
          cb(response.data.meta.msg);
        },
        (response) => {
          errorCb(response);
        },
      );
  },
};
