**Ideas**
Alvita thinks she is too stressed and this is causing her heart to race. She also wonders if her blood pressure tablet could be causing the symptoms.

**Concerns**
Alvita is very worried she may have a heart attack like her grandfather did, she is worried she cannot continue to work if she keeps getting dizzy.

**Expectations**
She would like a test to see if her heart is ok and to know if she can work.
