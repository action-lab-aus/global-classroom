**Immediate Management**
Alvita has a phone consultation as planned with Dr Lemmer to discuss her blood results, diagnosis and management options. She has not had further symptoms since yesterday.

**Ideas**
Alvita is worried that her symptoms will return. She thinks it might be the blood pressure tablet causing the symptoms.

**Concerns**
Alvita is still very worried she may have a heart attack like her grandfather did, she is worried she may not be allowed to work and is terrified she will need to go into hospital.

**Expectations**
She wants her GP to give her the results of her tests and to make her better.

**Further Examination**
Alvita has her own blood pressure monitor at home. She takes her own blood pressure and pulse prior to the appointment and informs Dr Lemmer that it was 138/83mmHg, and her pulse was regular and 85 beats per minute. She has not had a fever.

Dr Lemmer discusses the diagnosis of paroxysmal atrial fibrillation with Alvita and how this might put her at a risk of stroke. She also discusses the link between high blood pressure and atrial fibrillation and advises her generally about stress and caffeine intake.

Dr Lemmer uses the CHA2DS2VASc tool to assess the likely risk for Alvita.
They then discuss the results and the management of atrial fibrillation. Dr Lemmer also completes the HAS-BLED tool.

Dr Lemmer gives Alvita some written information and arranges to telephone her for a follow up after she’s had some time to consider her options.

Alvita has a follow up telephone appointment one week later. She takes her own blood pressure and pulse prior to the appointment and informs Dr Lemmer that it was 138/76mmHg, and her pulse was regular and 72 beats per minute. She has decided that she would like to start on anticoagulation. She has thought hard about the risks of not taking anticoagulants. Dr Lemmer explains that she does not need any other investigations or to go to hospital for an ECHO at present, and suggests that she starts on Apixaban. They discuss that her blood pressure is well controlled, but if she gets lots of episodes of AF a trial of different blood pressure medication might be considered, and if she develops persistent AF then she would need to consider different treatment. Dr Lemmer discusses stroke awareness, driving and medication and follow up.
