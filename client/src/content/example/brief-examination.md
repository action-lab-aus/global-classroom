**General examination (observations & vital signs)**
Temperature on arrival 36.7
Looks well but anxious
walking normally with normal gait.

**Cardiovascular system (CVS)**
No cyanosis
No scars
Pulse is 115 bpm and irregularly irregular
BP 147/92mmHg
Apex not displaced
Heart Sounds irregular rhythm but no added sounds (no murmurs heard)

**Respiratory system (RS)**
Respiratory rate 16 breaths per minute
Saturation on air 99%
Chest examination normal (chest expansion equal both sides, normal percussion and normal vesicular breath sounds)

**Abdomen**
Soft, non-tender, no organomegaly, no masses

**Central nervous system (CNS)**
Grossly normal
Walking and talking normally
Balance normal on walking standing and sitting

**Musculoskeletal (MSK)**
Not examined

**Head & Neck**
Ears normal
Eyes normal
No signs of goitre

**Mental state examination**
Appropriate dress
Normal speech and content of speech
Anxious about current problem
Good eye contact
Normal affect
Good insight
