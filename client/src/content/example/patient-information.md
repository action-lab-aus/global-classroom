**Family Name** - Johnson

**First Name** - Alvita

**Date of Birth** - 02.05.1967

**Age** - 53

**Ethnicity** - Black Caribbean

**Presentation Time** - 9.00

**Presentation Date** - 18th November 2020

**Gender** - Female

**Primary Clinician** - Dr Diane Lemmer (GP)
