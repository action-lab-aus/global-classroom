import { createRouter, createWebHistory } from "vue-router";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/login",
      name: "login",
      component: () => import("../views/LoginView.vue"),
    },
    {
      path: "/",
      name: "dashboard",
      component: () => import("../views/DashboardView.vue"),
    },
    {
      path: "/schedule",
      name: "schedule",
      component: () => import("../views/ScheduleView.vue"),
    },
    {
      path: "/documentation",
      name: "documentation",
      component: () => import("../views/DocumentationView.vue"),
    },
    {
      path: "/team/:id",
      name: "team",
      component: () => import("../views/TeamView.vue"),
    },
  ],
});

export default router;
