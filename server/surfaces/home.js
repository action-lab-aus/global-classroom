// Home
// Controls what is shown in the Slack app 'home tab' for each user

const fs = require('fs');
const YAML = require('yaml');

const slackifyMarkdown = require('slackify-markdown');

// Import required models
const { User } = require('../models');

const blockHelper = require('../block-helper');

const blocksForIntro = function (dbUser) {
  const welcomeMessage = fs.readFileSync('./content/welcome-message.md', 'utf8');

  return [
    blockHelper.headerBlock(`Hey ${dbUser.real_name} :wave:`),
    blockHelper.markdownBlock(slackifyMarkdown(welcomeMessage)),
  ];
};

const blocksForOnboarding = function (dbUser) {
  const onboardingMessage = fs.readFileSync('./content/onboarding-message.md', 'utf8');
  const i18n = YAML.parse(fs.readFileSync('./i18n.yaml', 'utf8'));

  const userTypeText = i18n.userTypes[dbUser.type].welcomeMessage;
  const roleText = i18n.roles[dbUser.role].welcomeMessage;

  let blocks = [blockHelper.headerBlock('Get started'), blockHelper.markdownBlock(userTypeText)];

  if (dbUser.hasEnrolled) {
    if (dbUser.type == 'student') {
      blocks.push(blockHelper.markdownBlock(roleText));
    }
  } else {
    blocks.push(
      blockHelper.markdownBlock(slackifyMarkdown(onboardingMessage)),
      blockHelper.buttonBlock('button_enrol', i18n.enrol.cta),
    );
  }
  return blocks;
};

const blocksForInformation = function (dbUser) {
  const courseInformation = fs.readFileSync('./content/course-information.md', 'utf8');

  if (!dbUser.hasEnrolled) {
    return [];
  } else {
    return [
      blockHelper.headerBlock('Information'),
      blockHelper.markdownBlock(slackifyMarkdown(courseInformation)),
    ];
  }
};

const blocksForAdminTools = function () {
  return [
    // Admin
    {
      type: 'actions',
      elements: [
        {
          type: 'button',
          style: 'primary',
          text: {
            type: 'plain_text',
            text: 'Launch admin tool',
            emoji: true,
          },
          url: process.env.CLIENT_URL,
        },
      ],
    },
  ];
};

const getBlocks = async function (user) {
  const appConfig = YAML.parse(fs.readFileSync('./config/app.yaml', 'utf8'));

  const dbUser = await User.findOne({ userId: user.user.id });

  let blocks = [];

  blocks = blocks.concat(blocksForIntro(dbUser));

  if (dbUser.type == 'student') {
    blocks = blocks.concat(blocksForOnboarding(dbUser));
  }

  blocks = blocks.concat(blocksForInformation(dbUser));

  // Check if admin
  if ((appConfig.admins ?? []).includes(dbUser.userId)) {
    blocks = blocks.concat(blocksForAdminTools());
  }
  return blocks;
};

const updateHomeView = async function (client, userId) {
  try {
    const user = await client.users.info({
      user: userId,
      include_locale: true,
    });

    // Push a view to the Home tab
    await client.views.publish({
      // ID of user that opened home tab
      user_id: userId,
      view: {
        type: 'home',
        callback_id: 'home_view',
        blocks: await getBlocks(user),
      },
    });
  } catch (error) {
    console.error(error);
  }
};

const init = function (app) {
  app.event('app_home_opened', async ({ event, client, context }) => {
    console.log('app_home_opened');

    const userId = event.user;

    await updateHomeView(client, userId);
  });
};

module.exports = {
  init,
  updateHomeView,
};
