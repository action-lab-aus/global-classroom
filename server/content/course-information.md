# Schedule

This pilot course will run from **December 1st 2022** through to **February 1st 2023**.

Here is a summary of what has/will happen:

- ~~Onboarding and enrollment~~
- Team assignment
- Ice-breaker
- Role assignment
- Patient introduction
- Visuals
- Team questions
- Visuals
- Team question awards
- Summary and resources

# Help

If you need help or have any questions during the pilot you can contact one of the support team by:

- Send a message to the support channel (please note everyone will be able to see this).
- Contact a course admin
