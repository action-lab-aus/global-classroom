// Upload
const middleware = require('../middleware.js');

module.exports = function (app, upload) {
  // Upload a file (for content) uses Multer
  app.post(
    '/api/upload',
    middleware.isAuthenticated(),
    upload.array('files', 5),
    function (req, res) {
      res.send([req.files[0].filename]);
    },
  );
};
