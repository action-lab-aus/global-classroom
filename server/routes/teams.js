// Teams
const middleware = require('../middleware.js');

// Import required models
const { User, Team } = require('../models');

module.exports = function (app, slackApp, orchestration, home) {
  // Retrieve all teams (not archived or deleted)
  app.get('/api/teams', middleware.isAuthenticated(), async (req, res) => {
    const teams = await Team.find({ archived: false, deleted: false }).sort({ created: -1 });

    for (const team of teams) {
      team.users = await User.find({ userId: team.users });
    }

    res.json({
      meta: { status: 200, msg: 'Found teams' },
      data: {
        teams,
      },
    });
  });

  // Retrieve a single team by ID
  app.get('/api/team/:id', middleware.isAuthenticated(), async (req, res) => {
    const team = await Team.findOne({ _id: req.params.id });

    res.json({
      meta: { status: 200, msg: 'Found team' },
      data: {
        team,
      },
    });
  });

  // Retrieve members for team by ID
  app.get('/api/team/:id/members', middleware.isAuthenticated(), async (req, res) => {
    const team = await Team.findOne({ _id: req.params.id });
    const members = await User.find({ userId: team.users });

    res.json({
      meta: { status: 200, msg: 'Team members found' },
      data: {
        team,
        members,
      },
    });
  });

  // Assign users to a team
  app.post('/api/teams/assign', middleware.isAuthenticated(), async (req, res) => {
    await orchestration.assignTeams(slackApp.client, home);

    res.json({
      meta: { status: 200, msg: 'Teams assigned' },
      data: {},
    });
  });

  // Unassign all users from all teams
  app.post('/api/teams/unassign', middleware.isAuthenticated(), async (req, res) => {
    await orchestration.unassignTeams(slackApp.client, home);

    res.json({
      meta: { status: 200, msg: 'Teams unassigned' },
      data: {},
    });
  });

  // Archive all teams
  app.post('/api/teams/archive', middleware.isAuthenticated(), async (req, res) => {
    await orchestration.archiveTeamChannels(slackApp.client);

    res.json({
      meta: { status: 200, msg: 'Team channels archived' },
      data: {},
    });
  });

  // Archive a single team by ID
  app.post('/api/teams/archive/:id', middleware.isAuthenticated(), async (req, res) => {
    let teamId = req.params.id;

    await orchestration.archiveTeamChannel(slackApp.client, teamId);

    res.json({
      meta: { status: 200, msg: 'Team channel archived' },
      data: {},
    });
  });

  // Delete all teams
  app.post('/api/teams/delete', middleware.isAuthenticated(), async (req, res) => {
    await orchestration.deleteTeamChannels(slackApp.client);

    res.json({
      meta: { status: 200, msg: 'Team channels deleted' },
      data: {},
    });
  });

  // CAUTION - Reset everything, delete teams, users, content etc.
  app.post('/api/reset/all', middleware.isAuthenticated(), async (req, res) => {
    await orchestration.resetAll(slackApp.client, home);

    res.json({
      meta: { status: 200, msg: 'Reset everything' },
      data: {},
    });
  });
};
