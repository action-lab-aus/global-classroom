// Image generator
// Builds urls for media endpoint - used to generate prompts, awards and question images

const YAML = require('yaml');
const fs = require('fs');

const { randomUUID } = require('crypto');

const contentTypes = YAML.parse(fs.readFileSync('./content/content-types.yaml', 'utf8'));

// Content prompts
const getPrompt = function (type, identifier) {
  const { title, subtitle, icon, leftColor, rightColor } = contentTypes[type];

  identifier = typeof identifier == undefined ? randomUUID() : identifier;

  const path = `prompt?title=${title}&subtitle=${subtitle}&leftColor=${leftColor}&rightColor=${rightColor}&icon=${icon}&identifier=${identifier}`;

  return encodeURI(path);
};

// Question awards
const getAward = function (award, team, identifier) {
  const { title, subtitle, icon } = award;
  const { channelId } = team;

  const path = `award?title=${title}&subtitle=${subtitle}&icon=${icon}&channelId=${channelId}&identifier=${identifier}`;

  return encodeURI(process.env.MEDIA_BASE_URL + path);
};

// Question prompts
const getQuestionPrompt = function (type, teamQuestion, team) {
  const { icon, leftColor, rightColor } = contentTypes[type];
  const title = `${team.name} asked...`;
  const subtitle = teamQuestion.question;

  const path = `prompt?title=${title}&subtitle=${subtitle}&leftColor=${leftColor}&rightColor=${rightColor}&icon=${icon}&identifier=${randomUUID()}`;

  return encodeURI(process.env.MEDIA_BASE_URL + path);
};

module.exports = {
  getPrompt,
  getAward,
  getQuestionPrompt,
};
