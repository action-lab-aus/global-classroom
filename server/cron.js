// Cron
// Periodically checks the database for scheduled items that need to be processed and sent to the Slack workspae

const fs = require('fs');

const cron = require('node-cron');

// Import required models
const { Poll, ScheduleItem, Team } = require('./models');

const blockHelper = require('./block-helper');
const imageGenerator = require('./image-generator');

module.exports = async function (slackApp, orchestration) {
  cron.schedule('*/5 * * * * *', async () => {
    console.log('Running cron task', Date.now());

    // Select one pending schedule item
    const now = parseInt(Date.now() / 1000);
    const item = await ScheduleItem.findOne({
      state: 'PENDING',
      postAt: { $lte: now },
    });

    // If there are no pending schedule items, abort
    if (typeof item == 'undefined') {
      console.log('No pending schedule items');
      return;
    }

    console.log('Pending schedule item found:', item.type, item.contentType);

    // Get all teams
    const allTeams = await Team.find({ archived: false, deleted: false });

    if (item.type == 'activity' && item.contentType == 'question-activity') {
      await scheduleQuestionActivity(item);
    }

    // Loop teams
    for (const team of allTeams) {
      console.log(`Processing schedule item for team: '${team.name}'`);

      // Used if a team hasn't received an award
      let skipSendingMessage = false;

      // Initialise blocks array
      let blocks = [];

      // Check if type is automated (e.g. question distribution and awards)
      if (item.type == 'automated') {
        // Automated

        // Add blocks
        var teamQuestionId = undefined;
        if (item.contentType == 'question-distribution') {
          const { teamQuestion, questionBlocks } = await blockHelper.questionDistributionBlocks(
            item,
            team,
          );
          // Add the question blocks
          blocks.push(...questionBlocks);
          // Set the question id
          teamQuestionId = teamQuestion._id;
        }

        if (item.contentType == 'question-awards') {
          const awardBlocks = await blockHelper.questionAwardsBlocks(team);
          if (awardBlocks.length == 0) {
            // No awards so skip sending the message
            skipSendingMessage = true;
          } else {
            // Add the award blocks
            blocks.push(...awardBlocks);
          }
        }
      } else {
        // Not automated
        console.log('Not automated');

        // Standard blocks
        blocks.push(...(await blockHelper.imageBlocks(item))); // Add images
        blocks.push(...(await blockHelper.textBlocks(item))); // Add text

        // Activities
        if (item.type == 'activity') {
          blocks.push(...(await blockHelper.activityBlocks(item, team))); // Activity blocks
        }
      }

      // Post content to team
      try {
        let message = undefined;

        if (!skipSendingMessage) {
          try {
            message = await orchestration.postMessage(slackApp.client, {
              channel: team.channelId,
              text: item.text ?? `${item.type} - ${item.contentType}`,
              blocks: blocks,
            });

            // Documents
            if (typeof item.documents != 'undefined') {
              await orchestration.uploadDocuments(slackApp, team, item.documents); // Upload documents
            }

          } catch (error) {
            console.error(error);
            await ScheduleItem.findOneAndUpdate({ _id: item._id }, { $set: { state: 'ERROR' } });
            return;
          }
        }

        if (typeof teamQuestionId != 'undefined') {
          // Store message timestamp and update state
          await ScheduleItem.findOneAndUpdate({ _id: item._id }, { $set: { state: 'POSTED' } });

          // Create new schedule item for posed question
          await ScheduleItem.create({
            type: 'automated',
            contentType: 'question-distribution',
            state: 'POSTED',
            'meta.teamQuestionId': teamQuestionId,
            timestamps: [message.ts],
          });
        } else {
          // Store message timestamp and update state
          let obj = { $set: { state: 'POSTED' } };
          if (typeof message != 'undefined') {
            obj['$addToSet'] = { timestamps: message.ts };
          }
          await ScheduleItem.findOneAndUpdate({ _id: item._id }, obj);

          // Poll timestamp
          await Poll.findOneAndUpdate({ scheduleItemId: item._id, teamId: team.channelId }, obj);
        }
      } catch (error) {
        console.error('Error', error);
      }
    }
  });
};

// Automated - schedule a question activity (quesiton distribution and awards)
const scheduleQuestionActivity = async function (item) {
  const dayjs = require('dayjs');
  const localizedFormat = require('dayjs/plugin/localizedFormat');
  dayjs.extend(localizedFormat);

  // Create the distribution item
  const questionsDistributionDateTime = dayjs.unix(item.meta.questionDistributionDate);
  const questionsDistribution = {
    time: questionsDistributionDateTime.format('L'),
    date: questionsDistributionDateTime.format('LT'),
  };

  // Create the awards item
  const awardsDistributionDateTime = dayjs.unix(item.meta.questionAwardsDate);
  const awardsDistribution = {
    time: awardsDistributionDateTime.format('L'),
    date: awardsDistributionDateTime.format('LT'),
  };

  try {
    // Create the distribution item
    await ScheduleItem.create({
      type: 'automated',
      contentType: 'question-distribution',
      state: 'PENDING',
      images: [imageGenerator.getPrompt('question-distribution')],
      text: `Questions will be distributed on ${questionsDistribution.time} at ${questionsDistribution.date}`,
      postAt: item.meta.questionDistributionDate,
    });

    // Create the awards item
    await ScheduleItem.create({
      type: 'automated',
      contentType: 'question-awards',
      state: 'PENDING',
      images: [imageGenerator.getPrompt('question-awards')],
      text: `Questions awards will be presented on ${awardsDistribution.time} at ${awardsDistribution.date}`,
      postAt: item.meta.questionAwardsDate,
    });
  } catch (error) {
    console.log('error', error);
  }
};
