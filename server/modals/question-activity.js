// Question activity

const YAML = require('yaml');
const fs = require('fs');

const i18n = YAML.parse(fs.readFileSync('./i18n.yaml', 'utf8'));

const { sentenceCase } = require('change-case');

const { TeamQuestion, User, Team } = require('../models');

const blockHelper = require('../block-helper');

const init = function (app, orchestration) {
  app.view('question_modal', async ({ ack, body, view, client, logger }) => {
    await ack();

    // Team question submitted
    const private_metadata = JSON.parse(view.private_metadata);

    // Store the question
    const teamQuestion = await TeamQuestion.create({
      teamId: private_metadata.channel_id,
      userId: private_metadata.user_id,
      question: view.state.values.question.question_input.value,
    });

    const team = await Team.findOne({ channelId: private_metadata.channel_id });

    const requestingUser = await User.findOne({ userId: private_metadata.user_id });

    // Let the team know the question was received
    await orchestration.postMessage(client, {
      channel: private_metadata.channel_id,
      text: i18n.questionActivity.receivedByTeam.plainText
        .replaceAll(/__REQUESTING_USER_REAL_NAME__/g, `<@${requestingUser.userId}>`)
        .replaceAll(/__TEAM_QUESTION__/g, teamQuestion.question),
      blocks: [
        blockHelper.markdownBlock(
          i18n.questionActivity.receivedByTeam.markdown
            .replaceAll(/__REQUESTING_USER_REAL_NAME__/g, `<@${requestingUser.userId}>`)
            .replaceAll(/__TEAM_QUESTION__/g, teamQuestion.question),
        ),
      ],
    });

    // Message admin with request
    // Get first admin
    try {
      const appConfig = YAML.parse(fs.readFileSync('./config/app.yaml', 'utf8'));
      const adminId = appConfig.admins[0];

      const requestMessage = await orchestration.postMessage(client, {
        channel: adminId,
        text: (() => {
          return i18n.questionActivity.receivedByAdmin.plainText
            .replaceAll(/__REQUESTING_USER_REAL_NAME__/g, `<@${requestingUser.userId}>`)
            .replaceAll(/__TEAM_NAME__/g, sentenceCase(team.name))
            .replaceAll(/__TEAM_QUESTION__/g, teamQuestion.question);
        })(),
        blocks: [
          blockHelper.markdownBlock(
            i18n.questionActivity.receivedByAdmin.markdown
              .replaceAll(/__REQUESTING_USER_REAL_NAME__/g, `<@${requestingUser.userId}>`)
              .replaceAll(/__TEAM_NAME__/g, sentenceCase(team.name))
              .replaceAll(/__TEAM_QUESTION__/g, teamQuestion.question),
          ),
          {
            type: 'actions',
            elements: [
              {
                type: 'button',
                style: 'primary',
                text: {
                  type: 'plain_text',
                  text: i18n.general.yes,
                  emoji: true,
                },
                value: 'accepted',
                action_id: 'button_team_question_accepted',
                confirm: {
                  title: {
                    type: 'plain_text',
                    text: i18n.general.confirmation,
                  },
                  text: {
                    type: 'plain_text',
                    text: i18n.questionActivity.modal.acceptWarning,
                  },
                  confirm: {
                    type: 'plain_text',
                    text: i18n.general.yes,
                  },
                  deny: {
                    type: 'plain_text',
                    text: i18n.general.cancel,
                  },
                },
              },
              {
                type: 'button',
                style: 'danger',
                text: {
                  type: 'plain_text',
                  text: i18n.general.no,
                  emoji: false,
                },
                value: 'rejected',
                action_id: 'button_team_question_rejected',
                confirm: {
                  title: {
                    type: 'plain_text',
                    text: i18n.general.confirmation,
                  },
                  text: {
                    type: 'mrkdwn',
                    text: i18n.questionActivity.modal.rejectWarning.replaceAll(
                      /__TEAM_NAME__/g,
                      sentenceCase(team.name),
                    ),
                  },
                  confirm: {
                    type: 'plain_text',
                    text: i18n.questionActivity.modal.rejectConfirmation,
                  },
                  deny: {
                    type: 'plain_text',
                    text: i18n.general.cancel,
                  },
                },
              },
            ],
          },
        ],
      });

      // Add request message to DB
      await TeamQuestion.findOneAndUpdate(
        { _id: teamQuestion._id },
        { requestMessage: requestMessage },
      );
    } catch (error) {
      console.error(error);
    }
  });

  app.command('/submit-team-question', async ({ ack, body, client, logger }) => {
    // Acknowledge the command request
    await ack();

    await showQuestionModal(client, body);
  });
};

const showQuestionModal = async function (client, body) {
  try {
    const result = await client.views.open({
      trigger_id: body.trigger_id,
      view: {
        type: 'modal',
        callback_id: 'question_modal',
        title: {
          type: 'plain_text',
          text: i18n.questionActivity.modal.title,
        },
        blocks: [
          {
            type: 'input',
            block_id: 'question',
            label: {
              type: 'plain_text',
              text: i18n.questionActivity.modal.prompt,
            },
            element: {
              type: 'plain_text_input',
              action_id: 'question_input',
              multiline: true,
            },
          },
        ],
        submit: {
          type: 'plain_text',
          text: i18n.general.submit,
        },
        private_metadata: JSON.stringify({
          channel_id: body.container.channel_id,
          user_id: body.user.id,
        }),
      },
    });
  } catch (error) {
    console.error(error);
  }
};

module.exports = {
  init,
  showQuestionModal,
};
