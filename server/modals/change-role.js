// Change role

const YAML = require('yaml');
const fs = require('fs');

// Import required models
const { Team, User, RoleRequest } = require('../models');

const i18n = YAML.parse(fs.readFileSync('./i18n.yaml', 'utf8'));
const blockHelper = require('../block-helper');

const init = function (app, orchestration) {
  // Handle a view_submission request
  app.view('change_role_modal', async ({ ack, body, view, client, logger }) => {
    // Acknowledge the view_submission request
    await ack();

    // Message the user
    try {
      const requestingUserId = body['user']['id'];
      const requestingUser = await User.findOne({ userId: requestingUserId });

      const private_metadata = JSON.parse(view.private_metadata);
      const role = view.state.values.role.role_input.selected_option.value; // Get requested role

      // Get team members
      const team = await Team.findOne({ channelId: private_metadata.channel_id });

      // Get user who currently holds the role 'targetUserId'
      const targetUser = await User.findOne({ role: role, userId: team.users });

      if (!targetUser) {
        console.error('User with role not found');
        return;
      }

      if (targetUser.userId == requestingUser.userId) {
        console.error('User is requesting their current role!');
        return;
      }

      if (targetUser) {
        // Message user who currently holds the role
        const requestMessage = await orchestration.postMessage(client, {
          channel: targetUser.userId,
          blocks: [
            blockHelper.markdownBlock(
              i18n.roleAssignment.changeModal.request
                .replaceAll(/TARGET__USER_REAL_NAME__/g, targetUser.real_name)
                .replaceAll(/__REQUESTING_USER_REAL_NAME__/g, `<@${requestingUser.userId}>`)
                .replaceAll(/__REQUESTING_USER_ROLE__/g, requestingUser.role)
                .replaceAll(/__ROLE__/g, role),
            ),
            {
              type: 'actions',
              elements: [
                {
                  type: 'button',
                  style: 'primary',
                  text: {
                    type: 'plain_text',
                    text: i18n.general.acceptRequest,
                    emoji: true,
                  },
                  value: 'accepted',
                  action_id: 'button_role_change_accepted',
                  confirm: {
                    title: {
                      type: 'plain_text',
                      text: i18n.general.confirmation,
                    },
                    text: {
                      type: 'mrkdwn',
                      text: (() => {
                        return i18n.roleAssignment.changeModal.requestAcceptWarning
                          .replaceAll(/__TARGET_USER_ROLE__/g, targetUser.role)
                          .replaceAll(/__REQUESTING_USER_ROLE__/g, requestingUser.role);
                      })(),
                    },
                    confirm: {
                      type: 'plain_text',
                      text: i18n.roleAssignment.changeModal.requestAcceptConfirmation,
                    },
                    deny: {
                      type: 'plain_text',
                      text: i18n.roleAssignment.changeModal.requestAcceptCancel,
                    },
                  },
                },
                {
                  type: 'button',
                  style: 'danger',
                  text: {
                    type: 'plain_text',
                    text: i18n.general.rejectRequest,
                    emoji: false,
                  },
                  value: 'rejected',
                  action_id: 'button_role_change_rejected',
                  confirm: {
                    title: {
                      type: 'plain_text',
                      text: i18n.general.confirmation,
                    },
                    text: {
                      type: 'mrkdwn',
                      text: (() => {
                        return i18n.roleAssignment.changeModal.requestRejectWarning
                          .replaceAll(/__TARGET_USER_ROLE__/g, targetUser.role)
                          .replaceAll(/__REQUESTING_USER_ROLE__/g, requestingUser.role);
                      })(),
                    },
                    confirm: {
                      type: 'plain_text',
                      text: i18n.roleAssignment.changeModal.requestRejectConfirmation,
                    },
                    deny: {
                      type: 'plain_text',
                      text: i18n.roleAssignment.changeModal.requestRejectCancel,
                    },
                  },
                },
              ],
            },
          ],
        });

        console.log('requestMessage', requestMessage);

        // Add request to DB
        await RoleRequest.create(
          {
            requestingUserId: requestingUser.userId,
            targetUserId: targetUser ? targetUser.userId : undefined,
            role: role,
            team: team.channelId,
            requestMessage: requestMessage,
          },
          { new: true },
        );
      } else {
        // Auto-assign the role
        await User.findOneAndUpdate({ slackId: requestingUserId }, { $set: { role: role } });

        orchestration.displayRoles(client, team.channelId);
      }
    } catch (error) {
      console.error(error);
    }
  });

  // Command - /request-role-change
  app.command('/request-role-change', async ({ command, ack, say, respond, client, body }) => {
    // Acknowledge command request
    await ack();

    await showModal(client, body);
  });
};

const showModal = async function (client, body) {
  const i18n = YAML.parse(fs.readFileSync('./i18n.yaml', 'utf8'));
  const user = await User.findOne({ userId: body['user_id'] });

  const roles = i18n.roles;
  let roleOptions = [];

  for (const identifier in roles) {
    const role = roles[identifier];
    if (identifier != user.role) {
      roleOptions.push({
        text: {
          type: 'plain_text',
          emoji: false,
          text: role.name,
        },
        value: identifier,
      });
    }
  }

  try {
    // Call views.open with the built-in client
    const result = await client.views.open({
      // Pass a valid trigger_id within 3 seconds of receiving it
      trigger_id: body.trigger_id,
      // View payload
      view: {
        type: 'modal',
        // View identifier
        callback_id: 'change_role_modal',
        title: {
          type: 'plain_text',
          text: i18n.roleAssignment.changeModal.title,
        },
        blocks: [
          blockHelper.markdownBlock(
            i18n.roleAssignment.changeModal.body
              .replaceAll(/__USER_REAL_NAME__/g, user.real_name)
              .replaceAll(/__USER_ROLE__/g, user.role),
          ),
          {
            type: 'input',
            block_id: 'role',
            label: {
              type: 'plain_text',
              text: i18n.roleAssignment.changeModal.prompt,
            },
            element: {
              action_id: 'role_input',
              type: 'static_select',
              placeholder: {
                type: 'plain_text',
                emoji: false,
                text: i18n.general.chooseAnOption,
              },
              options: roleOptions,
            },
          },
        ],
        submit: {
          type: 'plain_text',
          text: i18n.general.requestChange,
        },
        private_metadata: JSON.stringify({
          channel_id: body.channel_id,
        }),
      },
    });
  } catch (error) {
    console.error(error);
  }
};

module.exports = {
  init,
};
