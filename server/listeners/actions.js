// Actions (e.g. button clicks)
// Respond to Slack actions

const YAML = require('yaml');
const fs = require('fs');

const i18n = YAML.parse(fs.readFileSync('./i18n.yaml', 'utf8'));

const { sentenceCase, paramCase } = require('change-case');

const {
  Poll,
  RoleRequest,
  ScheduleItem,
  Team,
  TeamNameRequest,
  TeamQuestion,
  User,
} = require('../models');

const blockHelper = require('../block-helper');

const init = async function (slackApp, onboarding, orchestration, questionActivity) {
  // Button - Enrol
  slackApp.action('button_enrol', async ({ ack, body, client }) => {
    console.log('Enrol button clicked');

    // Acknowledge the command request
    await ack();

    await onboarding.showEnrolModal(client, body);
  });

  // Button - Team question
  slackApp.action('button_team_question', async ({ ack, body, client }) => {
    console.log('Team question');

    // Acknowledge the command request
    await ack();

    await questionActivity.showQuestionModal(client, body);
  });

  // Button / Radio button - Poll response
  slackApp.action('button_poll', async ({ ack, body }) => {
    console.log('Poll response submitted');

    // Acknowledge the command request
    await ack();

    // Find the related poll item
    const poll = await Poll.findOne({ timestamps: body.message.ts });

    // Find the related schedule item
    const item = await ScheduleItem.findOne({ _id: poll.scheduleItemId });

    // Get poll responses
    let responses = poll.responses;

    // Add user's response to the existing poll responses
    poll.responses[body.user.id] = body.actions[0].value;

    // Update poll responses in DB
    await Poll.findOneAndUpdate(
      { _id: poll._id },
      { $set: { responses: poll.responses } },
      { new: true },
    );

    // Update poll block with responses
    const blocks = [...(await blockHelper.pollBlocks(item, poll, responses))];

    // Update message
    await slackApp.client.chat.update({
      channel: body.container.channel_id,
      ts: body.message.ts,
      blocks: blocks,
      text: body.message.text,
    });
  });

  // Button - Team question accepted
  slackApp.action('button_team_question_accepted', async ({ ack, body, client, logger }) => {
    console.log('Button team question accepted clicked');

    // Acknowledge the command request
    await ack();

    // Update request in DB
    const teamQuestion = await TeamQuestion.findOneAndUpdate(
      { 'requestMessage.ts': body.container.message_ts },
      { $set: { state: 'ACCEPTED' } },
      { new: true },
    );

    try {
      // Delete request message
      await client.chat.delete({
        channel: teamQuestion.requestMessage.channel,
        ts: teamQuestion.requestMessage.ts,
      });
    } catch (error) {
      console.error(error);
    }

    try {
      // Inform teams
      await orchestration.postMessage(client, {
        channel: teamQuestion.teamId,
        text: i18n.questionActivity.modal.submissionFeedback,
        blocks: [
          blockHelper.markdownBlock(
            i18n.questionActivity.modal.submissionFeedback.replaceAll(
              /__TEAM_QUESTION__/g,
              teamQuestion.question,
            ),
          ),
        ],
      });
    } catch (error) {
      console.error(error);
    }
  });

  // Button - Team question rejected
  slackApp.action('button_team_question_rejected', async ({ ack, body, client, logger }) => {
    console.log('Button team question rejected clicked');

    // Acknowledge the command request
    await ack();

    // Update request in DB
    const teamQuestion = await TeamQuestion.findOneAndUpdate(
      { 'requestMessage.ts': body.container.message_ts },
      { $set: { state: 'REJECTED' } },
      { new: true },
    );

    try {
      // Delete request message
      await client.chat.delete({
        channel: teamQuestion.requestMessage.channel,
        ts: teamQuestion.requestMessage.ts,
      });
    } catch (error) {
      console.error(error);
    }

    try {
      // Inform teams
      await orchestration.postMessage(client, {
        channel: teamQuestion.teamId,
        text: i18n.questionActivity.modal.rejected,
        blocks: [blockHelper.markdownBlock(i18n.questionActivity.modal.rejected)],
      });
    } catch (error) {
      console.error(error);
    }
  });

  // Button - Team name change accepted
  slackApp.action('button_team_name_change_accepted', async ({ ack, body, client, logger }) => {
    console.log('Button team name change accepted clicked');

    // Acknowledge the command request
    await ack();

    // Update request in DB
    const teamNameRequest = await TeamNameRequest.findOneAndUpdate(
      { 'requestMessage.ts': body.container.message_ts },
      { $set: { state: 'ACCEPTED' } },
      { new: true },
    );

    // Delete request message
    // await client.chat.delete({
    //   channel: teamNameRequest.requestMessage.channel,
    //   ts: teamNameRequest.requestMessage.ts,
    // });

    // Rename team
    await orchestration.renameChannel(
      client,
      teamNameRequest.channelId,
      paramCase(teamNameRequest.newName),
    );

    // Inform teams
    const requestingUser = await User.findOne({ userId: teamNameRequest.requestingUserId });

    var responseText = i18n.teamName.changeModal.requestAccepted
      .replaceAll(/__REQUESTING_USER_REAL_NAME__/g, `<@${requestingUser.real_name}>`)
      .replaceAll(/__REQUESTED_NEW_TEAM_NAME__/g, teamNameRequest.newName);

    try {
      await orchestration.postMessage(client, {
        channel: teamNameRequest.channelId,
        text: responseText,
        blocks: [blockHelper.markdownBlock(responseText)],
      });
    } catch (error) {
      console.error(error);
    }
  });

  // Button - Team name change rejected
  slackApp.action('button_team_name_change_rejected', async ({ ack, body, client, logger }) => {
    console.log('Button team name change rejected clicked');

    // Acknowledge the command request
    await ack();

    // Update request in DB
    const teamNameRequest = await TeamNameRequest.findOneAndUpdate(
      { 'requestMessage.ts': body.container.message_ts },
      { $set: { state: 'REJECTED' } },
      { new: true },
    );

    // Delete request message
    await client.chat.delete({
      channel: teamNameRequest.requestMessage.channel,
      ts: teamNameRequest.requestMessage.ts,
    });

    // Inform team
    const team = await Team.findOne({ channelId: teamNameRequest.channelId });
    const requestingUser = await User.findOne({ userId: teamNameRequest.requestingUserId });

    var responseText = i18n.teamName.changeModal.requestRejected
      .replaceAll(/__REQUESTING_USER_REAL_NAME__/g, `<@${requestingUser.userId}>`)
      .replaceAll(/__REQUESTED_NEW_TEAM_NAME__/g, sentenceCase(team.name));

    try {
      await orchestration.postMessage(client, {
        channel: teamNameRequest.channelId,
        text: responseText,
        blocks: [blockHelper.markdownBlock(responseText)],
      });
    } catch (error) {
      console.error(error);
    }
  });

  // Button - Role change accepted
  slackApp.action('button_role_change_accepted', async ({ ack, body, client, logger }) => {
    console.log('Button role change accepted clicked');

    // Acknowledge the command request
    await ack();

    // Update request to ACCEPTED
    const roleRequest = await RoleRequest.findOneAndUpdate(
      { targetUserId: body.user.id, 'requestMessage.ts': body.container.message_ts },
      { $set: { state: 'ACCEPTED' } },
      { new: true },
    );

    // Swap roles
    const requestingUser = await User.findOne({ userId: roleRequest.requestingUserId });
    await User.findOneAndUpdate(
      { userId: roleRequest.targetUserId },
      { $set: { role: requestingUser.role } },
    );
    await User.findOneAndUpdate(
      { userId: requestingUser.userId },
      { $set: { role: roleRequest.role } },
    );

    // Delete request message
    // await client.chat.delete({
    //   channel: roleRequest.requestMessage.channel,
    //   ts: roleRequest.requestMessage.ts,
    // });

    // Show team new roles
    teams.displayRoles(client, roleRequest.team);
  });

  // Button - Role change rejected
  slackApp.action('button_role_change_rejected', async ({ ack, body, client, logger }) => {
    console.log('Button role change rejected clicked');

    // Acknowledge the command request
    await ack();

    // Update request to REJECTED
    const roleRequest = await RoleRequest.findOneAndUpdate(
      { targetUserId: body.user.id, 'requestMessage.ts': body.container.message_ts },
      { $set: { state: 'REJECTED' } },
      { new: true },
    );

    const requestingUser = await User.findOne({ userId: roleRequest.requestingUserId });
    const targetUser = await User.findOne({ userId: roleRequest.targetUserId });

    var responseText = i18n.teamName.changeModal.requestAccepted
      .replaceAll(/__REQUESTING___USER_REAL_NAME____/g, `<@${requestingUser.userId}>`)
      .replaceAll(/TARGET___USER_REAL_NAME__/g, targetUser.real_name)
      .replaceAll(/__REQUESTING_USER_ROLE__/g, requestingUser.role);

    try {
      // Inform the requesting user
      await orchestration.postMessage(client, {
        channel: requestingUser.userId,
        text: responseText,
      });
    } catch (error) {
      console.error(error);
    }

    try {
      // Delete request message
      await client.chat.delete({
        channel: roleRequest.requestMessage.channel,
        ts: roleRequest.requestMessage.ts,
      });
    } catch (error) {
      console.error(error);
    }
  });
};

module.exports = {
  init,
};
