const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// create TeamNameRequest Schema
const TeamNameRequest = new Schema(
  {
    requestingUserId: String,
    channelId: String,
    newName: String,
    requestMessage: {
      type: Object,
      default: {},
    },
    state: {
      type: String,
      default: 'REQUESTED',
    },
  },
  {
    timestamps: { createdAt: 'created', updatedAt: 'updated' },
    usePushEach: true,
  },
);

module.exports = mongoose.model('TeamNameRequest', TeamNameRequest);
