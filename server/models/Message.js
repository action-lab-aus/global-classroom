const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// create Message Schema
const Message = new Schema(
  {
    messageId: String,
    channelId: String,
    channelType: String,
    raw: {
      type: Object,
      default: {},
    },
  },
  {
    timestamps: { createdAt: 'created', updatedAt: 'updated' },
    usePushEach: true,
  },
);

module.exports = mongoose.model('Message', Message);
