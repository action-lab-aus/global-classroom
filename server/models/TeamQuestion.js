const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// create TeamQuestion Schema
const TeamQuestion = new Schema(
  {
    teamId: String,
    userId: String,
    question: String,
    requestMessage: {
      type: Object,
      default: {},
    },
    timesDistributed: {
      type: Number,
      default: 0,
    },
    state: {
      type: String,
      default: 'REQUESTED',
    },
    votes: {
      type: Array,
      default: [],
    },
    award: {
      type: String,
      default: 'none',
    },
  },
  {
    timestamps: { createdAt: 'created', updatedAt: 'updated' },
    usePushEach: true,
  },
);

module.exports = mongoose.model('TeamQuestion', TeamQuestion);
