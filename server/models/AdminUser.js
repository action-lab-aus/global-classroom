const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// create AdminUser Schema
const AdminUser = new Schema(
  {
    slackId: String,
    raw: {
      type: Object,
      default: {},
    },
  },
  {
    timestamps: { createdAt: 'created', updatedAt: 'updated' },
    usePushEach: true,
  },
);

module.exports = mongoose.model('AdminUser', AdminUser);
