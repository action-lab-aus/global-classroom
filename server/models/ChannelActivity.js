const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// create ChannelActivity Schema
const ChannelActivity = new Schema(
  {
    type: String,
    ChannelActivity: String,
    user: String,
    channel: String,
    channel_type: String,
    team: String,
    inviter: String,
  },
  {
    timestamps: { createdAt: 'created', updatedAt: 'updated' },
    usePushEach: true,
  },
);

module.exports = mongoose.model('ChannelActivity', ChannelActivity);
