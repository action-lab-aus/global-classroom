// Block helper
// Abstraction for creating Slack 'blocks' when sending messages

const YAML = require('yaml');
const fs = require('fs');

const i18n = YAML.parse(fs.readFileSync('./i18n.yaml', 'utf8'));
const questionAwards = YAML.parse(fs.readFileSync('./content/question-awards.yaml', 'utf8'));

const { sentenceCase } = require('change-case');
const { randomUUID } = require('crypto');

// Import required models
const { Team, User, TeamQuestion, Poll } = require('./models');

const imageGenerator = require('./image-generator');

const blockType = {
  HEADER: 'header',
  SECTION: 'section',
  ACTIONS: 'actions',
  BUTTON: 'button',
  IMAGE: 'image',
  DIVIDER: 'divider',
};

const blockTextType = {
  PLAIN: 'plain_text',
  MRKDWN: 'mrkdwn',
};

const accessoryType = {
  BUTTON: 'button',
};

const headerBlock = function (text) {
  return {
    type: blockType.HEADER,
    text: {
      type: blockTextType.PLAIN,
      text: text,
    },
  };
};

const dividerBlock = function () {
  return {
    type: blockType.DIVIDER,
  };
};

const markdownBlock = function (text) {
  return {
    type: blockType.SECTION,
    text: {
      type: blockTextType.MRKDWN,
      text: text,
    },
  };
};

const buttonBlock = function (action_id, text) {
  return {
    type: blockType.ACTIONS,
    elements: [
      {
        type: blockType.BUTTON,
        text: {
          type: blockTextType.PLAIN,
          text: text,
        },
        action_id: action_id,
      },
    ],
  };
};

const imageBlock = function (image_url, alt_text) {
  return {
    type: blockType.IMAGE,
    image_url: image_url,
    alt_text: alt_text,
  };
};

const textBlocks = async function (item) {
  // Check there is text to add
  if (typeof item.text == 'undefined' || item.text.length === 0) {
    return []; // No text, return empty array
  }

  // If this is an activity or an automated schedule item, do not add the text block
  if (['activity', 'automated'].includes(item.type)) {
    return []; // Return empty array
  }

  // Return the text block
  return [markdownBlock(item.mrkdwnText.substring(0, 3000))];
};

const imageBlocks = async function (item) {
  // Check there are images to add
  if (typeof item.images == 'undefined') {
    return []; // No images, return empty array
  }

  // Return the image blocks
  return item.images.map((i) => {
    return imageBlock(`${process.env.MEDIA_BASE_URL}${i}`, 'Course image');
  });
};

const activityBlocks = async function (item, team) {
  let blocks = [];

  // Poll
  if (item.contentType == 'poll') {
    blocks.push(...(await pollBlocks(item, team)));
  }

  // Ice breaker
  if (item.contentType == 'ice-breaker') {
    blocks.push(...(await iceBreakerBlocks(team)));
  }

  // Role assignment
  if (item.contentType == 'role-assignment') {
    blocks.push(...(await roleAssignmentBlocks(team)));
  }

  // Question activity
  if (item.contentType == 'question-activity') {
    blocks.push(...(await questionActivityBlocks(item)));
  }

  return blocks;
};

const pollBlocks = async function (item, team, responses = []) {
  let blocks = [];

  blocks.push(
    headerBlock(i18n.poll.header)
  );

  // Create poll if it doesn't exist
  const poll = await Poll.findOneAndUpdate(
    { scheduleItemId: item._id, teamId: team.channelId },
    {
      scheduleItemId: item._id,
      teamId: team.channelId,
      question: item.text,
      options: item.pollOptions,
    },
    { new: true, upsert: true },
  );

  // Add text
  if (poll.question) {
    blocks.push(headerBlock(poll.question));
  }

  // Add poll
  let index = 0;
  for (const option of poll.options) {
    blocks.push({
      type: blockType.SECTION,
      text: {
        type: blockTextType.MRKDWN,
        text: option,
      },
      accessory: {
        type: accessoryType.BUTTON,
        text: {
          type: blockTextType.PLAIN,
          text: i18n.general.vote,
          emoji: true,
        },
        value: `${index}`,
        action_id: 'button_poll',
      },
    });

    // Votes - context
    let context = {
      type: 'context',
      elements: [],
    };

    for (const response in responses) {
      if (responses[response] == index) {
        const user = await User.findOne({ userId: response });
        context.elements.push(imageBlock(user.image, user.real_name));
      }
    }

    let count = 0;
    Object.keys(responses).forEach((key) => {
      if (responses[key] == index) {
        count++;
      }
    });

    context.elements.push({
      type: 'mrkdwn',
      text: count == 0 ? 'No votes' : count == 1 ? `${count} vote` : `${count} votes`,
    });

    blocks.push(dividerBlock());

    blocks.push(context);

    index++;
  }

  return blocks;
};

const iceBreakerBlocks = async function (team) {
  let blocks = [];

  try {
    blocks.push(
      headerBlock(i18n.iceBreaker.header),
      markdownBlock(
        i18n.iceBreaker.message
          .replaceAll(/__TEAM_COUNT__/g, team.userCount)
          .replaceAll(/__TEAM_NAME__/g, team.name)
          .replaceAll(/__CHANGE_TEAM_NAME_COMMAND__/g, '/change-team-name'),
      ),
    );

    // Update DB
    await Team.findOneAndUpdate(
      { channelId: team.channelId },
      { $set: { received_onboarding: true } },
      { upsert: false },
    );

    console.log('Team received onboarding/ice-breaker updated in DB');
  } catch (error) {
    console.error(error);
  }
  return blocks;
};

const roleAssignmentBlocks = async function (team) {
  let blocks = [];

  blocks.push(headerBlock(i18n.roleAssignment.header));

  // Loop teams
  if (typeof team != 'undefined') {
    let roleSummary = '';

    // Loop team members
    for (const index in team.users) {
      let user = await User.findOne({ userId: team.users[index] });

      if(user.type !== 'student') continue;

      const users = await User.find({ userId: team.users, type: 'student' });
      const takenRoles = users.map((u) => u.role);

      let role = user.role;

      let availableRoles = new Set(Object.keys(i18n.roles));

      availableRoles.delete('none');

      availableRoles = Array.from(availableRoles);

      // Find a role
      while (!availableRoles.includes(role)) {
        // Try leader role
        if (!takenRoles.includes('leader')) {
          role = 'leader';
          continue;
        }

        // Try writer role
        if (!takenRoles.includes('writer')) {
          role = 'writer';
          continue;
        }

        // Try researcher role
        if (!takenRoles.includes('researcher')) {
          role = 'researcher';
          continue;
        }

        // Get number of remaining assigned roles
        const researchers = takenRoles.filter((r) => r == 'researcher').length;
        const evaluators = takenRoles.filter((r) => r == 'evaluator').length;

        if (evaluators > researchers) {
          role = 'researcher';
        } else {
          role = 'evaluator';
        }
      }

      // Assign role
      user = await User.findOneAndUpdate(
        { userId: user.userId },
        { $set: { role: role } },
        { new: true },
      );

      // Update summary
      roleSummary = roleSummary + `\n*${user.real_name}* - ${user.role}`;
    }

    let roleDescriptions = '';

    for (const key in i18n.roles) {
      if (key != 'none') {
        roleDescriptions =
          roleDescriptions + `*${i18n.roles[key].name}*\n_${i18n.roles[key].description}_\n\n`;
      }
    }

    blocks.push(
      markdownBlock(
        i18n.roleAssignment.summary
          .replaceAll(/__ROLE_DESCRIPTIONS__/g, roleDescriptions)
          .replaceAll(/__REQUEST_ROLE_CHANGE_COMMAND__/g, '/request-role-change'),
      ),
      headerBlock(i18n.roleAssignment.summaryHeader),
      markdownBlock(roleSummary),
    );

    return blocks;
  }
};

const questionActivityBlocks = async function (item) {
  let blocks = [];

  const dayjs = require('dayjs');

  const relativeTime = require('dayjs/plugin/relativeTime');
  dayjs.extend(relativeTime);

  const submissionDeadline = dayjs(item.meta.questionSubmissionDeadline * 1000);
  const timeUntilSubmissionDeadline = dayjs(item.meta.questionSubmissionDeadline * 1000).fromNow();

  const localizedFormat = require('dayjs/plugin/localizedFormat');
  dayjs.extend(localizedFormat);

  blocks.push(
    markdownBlock(item.mrkdwnText),
    markdownBlock(
      i18n.questionActivity.submitMessage
        .replaceAll(/__SUBMISSION_DEADLINE__/g, submissionDeadline)
        .replaceAll(/__TIME_UNTIL_SUBMISSION_DEADLINE__/g, timeUntilSubmissionDeadline),
    ),
    buttonBlock('button_team_question', i18n.questionActivity.submitButton),
  );

  return blocks;
};

const questionAwardsBlocks = async function (team) {
  let blocks = [];

  // Load awards from YAML
  const awards = Object.keys(questionAwards['question-awards']).map((key) => {
    return questionAwards['question-awards'][key];
  });

  // Calculate awards based on emoji reactions
  // Get all questions
  const teamQuestions = await TeamQuestion.find({ award: 'none' });

  // Loop awards
  for (const award of awards) {
    console.log('Processing award:', award.id);

    let highestVotes = 0;
    let winningTeamId = undefined;
    let winningQuestionId = undefined;

    // Loop questions
    teamQuestions.forEach((question) => {
      // Get question votes with given award emoji
      let votes = (question.votes ?? []).filter((v) => {
        let voteElements = v.split('-');
        // let voteUser = voteElements[0];
        let voteEmoji = voteElements[1];
        return voteEmoji.includes(award.emoji);
      });

      if (votes.length > highestVotes) {
        highestVotes = votes.length;
        winningTeamId = question.teamId;
        winningQuestionId = question._id;
      }
    });

    if (team.channelId == winningTeamId) {
      await TeamQuestion.findOneAndUpdate(
        { _id: winningQuestionId },
        { $set: { award: award.id } },
        { upsert: false },
      );

      const awardURI = imageGenerator.getAward(award, team, randomUUID());

      blocks.push(
        imageBlock(awardURI, 'Award image'),
        markdownBlock(`:${award.emoji}: ${award.description}`),
      );
    }
  }

  console.log('blocks', blocks);

  return blocks;
};

const questionDistributionBlocks = async function (item, team) {
  let questionBlocks = [];

  // Get active teams
  const activeTeams = await Team.find({ archived: false, deleted: false });
  let activeTeamIds = new Set(activeTeams.map((t) => t.channelId));
  activeTeamIds.delete(team.channelId); // Remove current team
  activeTeamIds = Array.from(activeTeamIds);

  // Select a question for the given team
  let teamQuestion = await TeamQuestion.findOneAndUpdate(
    {
      teamId: { $in: activeTeamIds },
      state: 'ACCEPTED',
    },
    { $inc: { timesDistributed: 1 } },
    { new: true },
  ).sort({
    timesDistributed: 1,
  });

  if (!teamQuestion) {
    // Send least answered question
    // TOOD: Could be their own question!
    teamQuestion = await TeamQuestion.findOneAndUpdate(
      {
        state: 'ACCEPTED'
      },
      { $inc: { timesDistributed: 1 } },
      { new: true },
    ).sort({
      timesDistributed: 1,
    });
  }

  if (!teamQuestion) {
    // no accepted questions for this team
    return;
  }

  const teamForQuestion = await Team.findOne({ channelId: teamQuestion.teamId });

  if (typeof item.contentType != 'undefined') {
    questionBlocks.push(
      imageBlock(
        imageGenerator.getQuestionPrompt(item.contentType, teamQuestion, teamForQuestion),
        'Question image',
      ),
    );
  }

  let voteOptions = [];
  for (const questionVoteType of i18n.questionActivity.voteTypes) {
    voteOptions.push(
      `:${questionVoteType.emoji}: *${questionVoteType.title}* - ${questionVoteType.description}`,
    );
  }
  voteOptions = voteOptions.join('\n\n');

  questionBlocks.push(
    markdownBlock(
      i18n.questionActivity.question
        .replaceAll(/__TEAM_QUESTION_QUESTION__/g, teamQuestion.question)
        .replaceAll(/__TEAM_FOR_QUESTION_NAME__/g, sentenceCase(teamForQuestion.name))
        .replaceAll(/__VOTE_OPTIONS__/g, voteOptions),
    ),
  );

  return { teamQuestion, questionBlocks };
};

module.exports = {
  headerBlock,
  dividerBlock,
  markdownBlock,
  buttonBlock,
  imageBlock,
  textBlocks,
  imageBlocks,
  activityBlocks,
  pollBlocks,
  questionAwardsBlocks,
  questionDistributionBlocks,
};
