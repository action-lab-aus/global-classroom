const YAML = require('yaml');
const fs = require('fs');
const i18n = YAML.parse(fs.readFileSync('./i18n.yaml', 'utf8'));

const teamNames = YAML.parse(fs.readFileSync('./team-names.yaml', 'utf8'));

const animal = require('animal-id');

animal.useAdjectives(teamNames.adjectives);
animal.useAnimals(teamNames.animals);

const blockHelper = require('./block-helper');

// Import required models
const {
  ChannelActivity,
  Message,
  Poll,
  RoleRequest,
  ScheduleItem,
  Team,
  TeamNameRequest,
  TeamQuestion,
  User,
} = require('./models');

const postMessage = async function (client, message) {
  console.log('Posting message...');

  const appConfig = YAML.parse(fs.readFileSync('./config/app.yaml', 'utf8'));

  // Set username
  message['username'] = appConfig.app.username || 'Global Classroom';

  // Set icon
  message['icon_url'] = `${process.env.MEDIA_BASE_URL}app/icon`;

  // Post message to Slack
  try {
    return await client.chat.postMessage(message);
  } catch (error) {
    console.error(error);
  }
};

// Upload documents to Slack
const uploadDocuments = async function (slackApp, team, documents) {
  console.log('Uploading documents', documents);
  try {
    for (const document of documents) {
      let fileUploadResult = await slackApp.client.files.upload({
        file: fs.createReadStream(`${process.env.PUBLIC_MEDIA_PATH}/${document}`),
        channels: team.channelId,
      });
      console.log('fileUploadResult', fileUploadResult);
    }
  } catch (error) {
    console.error('error', error);
  }
};

const displayRoles = async function (client, channelId) {
  // Get team
  const team = await Team.findOne({ channelId: channelId });

  // Get team members
  const teamMembers = await User.find({ userId: team.users });

  // Let the team know the new roles
  let roleSummary = 'The new roles are:\n';

  for (const user of teamMembers) {
    // Update summary
    roleSummary = roleSummary + `\n*${user.real_name}* - ${user.role}`;
  }

  // Message team
  await self.postMessage(client, {
    channel: team.channelId,
    text: i18n.roleAssignment.rolesChanged.plain_text,
    blocks: [
      blockHelper.markdownBlock(i18n.roleAssignment.rolesChanged.markdown),
      blockHelper.markdownBlock(roleSummary),
    ],
  });
};

const unassignTeams = async function (client, home) {
  // Archive team channels
  await archiveTeamChannels(client);

  // Remove users from teams
  await Team.updateMany({}, { $set: { users: [], userCount: 0 } });
  await User.updateMany({}, { $set: { assignedTeam: false }, $unset: { role: 1 } });

  // Update user home tabs
  let users = await User.find({});
  for (const user of users) {
    await home.updateHomeView(client, user.userId);
  }
};

const assignTeams = async function (client, home) {
  const appConfig = YAML.parse(fs.readFileSync('./config/app.yaml', 'utf8'));

  var assigningTeams = true;

  while (assigningTeams) {
    let userIdsSet = new Set();

    const excludedUserIds = [
      ...(appConfig.admins ?? []),
      ...(appConfig.facilitators ?? []),
      ...(appConfig.bots ?? []),
    ];

    const teamUsers = await User.find()
      .where('assignedTeam')
      .equals(false)
      .where('deleted')
      .equals(false)
      .where('userId')
      .nin(excludedUserIds)
      .sort({ created: -1 })
      .limit(appConfig.teams.idealSize);

    console.log('teamUsers', teamUsers);

    for (const teamUser of teamUsers) {
      userIdsSet.add(teamUser.userId);
    }

    const atLeastOneTeamExists =
      (await (await Team.find({ archived: false, deleted: false })).length) > 0;

    if (atLeastOneTeamExists && userIdsSet.size < appConfig.teams.idealSize) {
      console.log(
        `Team assignment halted, remaining users ${userIdsSet.size}, min team size is ${appConfig.teams.minSize}`,
      );
      assigningTeams = false;

      for (const userId of userIdsSet) {
        console.log('remaining userId', userId);
        let smallestTeam = await Team.findOne({ archived: false, deleted: false }).sort({
          userCount: -1,
        });
        console.log('smallestTeam', smallestTeam.name);
        inviteUsersToTeamChannel(client, smallestTeam.channelId, [userId], home);
      }

      console.log('All users assigned to teams');
    } else {
      try {
        // Remove current user before inviting
        try {
          let currentUserId = await client.auth.test({});
          console.log('currentUserId', currentUserId.user_id);
          userIdsSet.delete(currentUserId.user_id);
        } catch (error) {
          console.error(error); // console.error(error.data.error);
        }

        let userIds = [...userIdsSet];

        var teamName = `the-${animal.getId()}s`; // https://www.npmjs.com/package/animal-id

        while (await Team.findOne({ name: teamName })) {
          teamName = `the-${animal.getId()}s`;
        }

        console.log(`Creating a new team channel with name '${teamName}' for users:`, userIds);
        await createTeamChannel(client, teamName, userIds, home);

        for (const teamUser of teamUsers) {
          await User.findOneAndUpdate(
            { userId: teamUser.userId },
            { $set: { assignedTeam: true } },
            { upsert: false },
          );

          await home.updateHomeView(client, teamUser.userId);
        }
      } catch (error) {
        console.error('error', error);
      }
    }
  }
  const teams = await Team.find({ archived: false, deleted: false });

  for (const team of teams) {
    // Add facilitators to all teams
    await inviteUsersToTeamChannel(client, team.channelId, appConfig.facilitators ?? [], home);
    // Add admins to all teams
    await inviteUsersToTeamChannel(client, team.channelId, appConfig.admins ?? [], home);
  }
};

// Kick all members except admins
const kickAllTeamMembers = async function (client) {
  const appConfig = YAML.parse(fs.readFileSync('./config/app.yaml', 'utf8'));

  // Get all team channels
  const result = await client.conversations.list({
    types: 'private_channel',
    exclude_archived: false,
  });

  // Loop channels
  for (const channel of result.channels) {
    let channelId = channel.id;

    try {
      await client.conversations.unarchive({
        channel: channelId,
      });
      console.log('Channel unarchive', channelId);
    } catch (error) {
      console.error('error - unarchive', error.data.error);
    }

    const memberResult = await client.conversations.members({
      channel: channelId,
    });

    for (const userId of memberResult.members) {
      if (!(appConfig.admins ?? []).includes(userId)) {
        try {
          await client.conversations.kick({
            channel: channelId,
            user: userId,
          });
        } catch (error) {
          console.error('error', error);
        }
      }
    }
  }
};

const inviteUsersToTeamChannel = async function (client, channelId, users, home) {
  console.log('Inviting users to channel', users);

  const appConfig = YAML.parse(fs.readFileSync('./config/app.yaml', 'utf8'));

  try {
    // Try invite users to existing channel
    const result = await client.conversations.invite({
      channel: channelId,
      users: users.join(','),
    });

    console.log('Users invited to channel', result);
  } catch (error) {
    console.error('error', error);
  }

  // Update DB
  try {
    let team = await Team.findOne({ channelId: channelId });
    let teamUsers = new Set();

    for (const userId of team.users) {
      teamUsers.add(userId);
    }
    for (const userId of users) {
      const excludedUserIds = [
        ...(appConfig.admins ?? []),
        ...(appConfig.facilitators ?? []),
        ...(appConfig.bots ?? []),
      ];

      if (!excludedUserIds.includes(userId)) {
        teamUsers.add(userId);
      }
    }

    for (const userId of teamUsers) {
      await home.updateHomeView(client, userId);
    }

    team = await Team.findOneAndUpdate(
      { channelId: channelId },
      { $set: { users: [...teamUsers], userCount: teamUsers.size } },
      { upsert: false, new: true },
    );

    console.log('Users invited to channel');
  } catch (err) {
    console.error(err);
  }
};

const getExistingChannelId = async function (client, name) {
  console.log('Checking if channel exists and getting id...', name);

  const result = await client.conversations.list({ types: 'private_channel' });

  for (const conversation of result.channels) {
    if (conversation['name'] === name) {
      console.log('Conversation exists, for name: ', name);
      return conversation['id'];
    } else {
      console.log('No match', conversation['name']);
    }
  }

  return undefined;
};

const createTeamChannel = async function (client, name, users, home) {
  let channelId = undefined;

  try {
    const result = await client.conversations.create({
      name: name,
      is_private: true,
      users: users,
    });
    channelId = result.channel.id;
    console.log('Channel successfully created with ID: ', channelId);

    // Add to database
    try {
      await Team.findOneAndUpdate(
        { channelId: channelId },
        { channelId: channelId, name: name, altNames: [name] },
        { upsert: true },
      );
      console.log('Team added to DB');
    } catch (err) {
      console.error(err);
    }
  } catch (error) {
    console.error('createTeamChannel error', JSON.stringify(error));

    // TODO: Check error is because channel exists
    channelId = await getExistingChannelId(client, name);
  }

  if (typeof channelId !== 'undefined') {
    // Invite users to channel
    try {
      await inviteUsersToTeamChannel(client, channelId, users, home);
    } catch (error) {
      console.error('Failed to invite users to channel', error);
    }
  } else {
    // Channel not created not inviting users
    console.error('Channel not created not inviting users');
  }
};

const archiveTeamChannels = async function (client, home) {
  console.log('Archiving all team channels');
  // Get all teams
  const teams = await Team.find({ archived: false, deleted: false });

  // Loop teams
  for (const team of teams) {
    try {
      // Try and archive the team's channel
      await client.conversations.archive({
        channel: team.channelId,
      });

      // Mark the team as archived and remove users
      await Team.updateMany(
        { channelId: team.channelId },
        { $set: { archived: true } },
        { $unset: { users: 1 } },
      );

      // Update team assignment and reset role
      await User.updateMany(
        { userId: team.users },
        { $set: { assignedTeam: false }, $unset: { role: 1 } },
      );

      // Update user home views
      for (const user of team.users) {
        await home.updateHomeView(client, user.userId);
      }

      console.log('Team channel archived');
    } catch (error) {
      console.error('error', error);
    }
  }
};

const archiveTeamChannel = async function (client, teamId) {
  console.log('Archiving team channel', teamId);

  // Get team channel
  const team = await Team.findOne({ channelId: teamId });

  try {
    // Try and archive the team's channel
    await client.conversations.archive({
      channel: team.channelId,
    });

    // Mark the team as archived and remove users
    await Team.updateMany(
      { channelId: team.channelId },
      { $set: { archived: true } },
      { $unset: { users: 1 } },
    );

    // Update team assignment and reset role
    await User.updateMany(
      { userId: team.users },
      { $set: { assignedTeam: false }, $unset: { role: 1 } },
    );

    console.log('Team channel archived');
  } catch (error) {
    console.error('error', error);
  }
};

const deleteTeamChannels = async function (client) {
  // Get all team channels
  await Team.deleteMany({});

  console.log('All team channels deleted');
};

const renameChannel = async function (client, channelId, newName) {
  console.log('Renaming channel', channelId, newName);
  try {
    // Try rename team channel
    const result = await client.conversations.rename({
      channel: channelId,
      name: newName,
    });

    // Update DB
    await Team.findOneAndUpdate(
      { channelId: channelId },
      { $set: { name: newName }, $addToSet: { altNames: newName } },
      { upsert: false },
    );

    console.log('Team name updated');
  } catch (error) {
    console.error('error', error);
  }
};

const resetAll = async function (client, home) {
  console.log('RESETTING ALL');

  // Archive the channels on Slack before removing from database
  await archiveTeamChannels(client, home);

  await Team.deleteMany({});
  await User.deleteMany({});
  await ChannelActivity.deleteMany({});
  await Message.deleteMany({});
  await Poll.deleteMany({});
  await RoleRequest.deleteMany({});
  await ScheduleItem.deleteMany({});
  await TeamNameRequest.deleteMany({});
  await TeamQuestion.deleteMany({});
};

module.exports = {
  postMessage,
  uploadDocuments,
  renameChannel,
  createTeamChannel,
  kickAllTeamMembers,
  inviteUsersToTeamChannel,
  assignTeams,
  unassignTeams,
  archiveTeamChannels,
  archiveTeamChannel,
  deleteTeamChannels,
  displayRoles,
  resetAll,
};
