// Import required models
const { Message } = require('./models');

const logMessage = async function (event) {
  // Store message in DB
  await Message.create({
    messageId: event.client_msg_id,
    channelId: event.channel,
    channelType: event.channel_type,
    raw: event,
  });
};

module.exports = {
  logMessage,
};
